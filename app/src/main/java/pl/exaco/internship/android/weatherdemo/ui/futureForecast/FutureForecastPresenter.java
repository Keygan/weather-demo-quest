package pl.exaco.internship.android.weatherdemo.ui.futureForecast;

import android.util.Log;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.service.ICitiesManager;
import pl.exaco.internship.android.weatherdemo.service.IFutureWeatherManager;
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory;
import pl.exaco.internship.android.weatherdemo.service.IWeatherManager;
import pl.exaco.internship.android.weatherdemo.service.RequestCallback;
import pl.exaco.internship.android.weatherdemo.ui.weather.WeatherContract;

/**
 * Created by jurki on 08.07.2018.
 */

public class FutureForecastPresenter implements FutureForecastContract.Presenter {
    private final FutureForecastContract.View view;
    private IFutureWeatherManager futureWeatherManager;

    public FutureForecastPresenter(FutureForecastContract.View view, IServiceFactory serviceFactory) {
        this.view=view;
        this.futureWeatherManager = serviceFactory.getFutureWeatherManager();

    }

    @Override
    public void getWeather(int cityId) {
        futureWeatherManager.getFutureWeatherForCity(cityId, new RequestCallback<List<CityWeather>>() {
            @Override
            public void onSuccess(List<CityWeather> data) {

                Log.e("CON","succesfull connection to api");
                view.onSuccess(data);
            }

            @Override
            public void onError(Throwable throwable) {
                Log.e("Err-Forecast-Presenter","Error fetching data");
                view.onFailure();
            }
        });
    }
}
