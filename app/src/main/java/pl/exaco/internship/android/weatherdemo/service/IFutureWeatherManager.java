package pl.exaco.internship.android.weatherdemo.service;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.City;
import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.service.RequestCallback;

public interface IFutureWeatherManager {

    void getFutureWeatherForCity(int cityId, RequestCallback<List<CityWeather>> callback);
}
