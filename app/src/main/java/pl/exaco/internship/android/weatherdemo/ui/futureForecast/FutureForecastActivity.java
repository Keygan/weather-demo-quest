package pl.exaco.internship.android.weatherdemo.ui.futureForecast;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.R;
import pl.exaco.internship.android.weatherdemo.databinding.ActivityCitiesBinding;
import pl.exaco.internship.android.weatherdemo.databinding.ActivityFutureForecastBinding;
import pl.exaco.internship.android.weatherdemo.model.City;
import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.model.FutureWeather;
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory;
import pl.exaco.internship.android.weatherdemo.service.impl.ServiceFactory;
import pl.exaco.internship.android.weatherdemo.ui.city.CityActivity_;
import pl.exaco.internship.android.weatherdemo.ui.weather.WeatherActivity;
import pl.exaco.internship.android.weatherdemo.ui.weather.WeatherAdapter;
import pl.exaco.internship.android.weatherdemo.ui.weather.WeatherContract;
import pl.exaco.internship.android.weatherdemo.ui.weather.WeatherPresenter;

@DataBound
@EActivity(R.layout.activity_future_forecast)
public class FutureForecastActivity extends AppCompatActivity implements FutureForecastContract.View {
    private static final String TAG = FutureForecastActivity.class.getSimpleName();

    @Bean(ServiceFactory.class)
    IServiceFactory serviceFactory;

    @BindingObject
    ActivityFutureForecastBinding binding;

    private FutureForecastAdapter adapter;
    private FutureForecastContract.Presenter presenter;
    private int id;

    @AfterViews
    void viewCreated() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FutureForecastAdapter(this);
        binding.recyclerView.setAdapter(adapter);
        presenter = new FutureForecastPresenter(this, serviceFactory);
        id = getIntent().getExtras().getInt("id");
        getData(id);
    }

    private void getData(int cityId) {
       binding.progressBar.setVisibility(View.VISIBLE);
       presenter.getWeather(cityId);
    }

    @Override
    public void onSuccess(List<CityWeather> data) {
        binding.progressBar.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            binding.recyclerView.setVisibility(View.VISIBLE);
            adapter.setItems(data);
        } else {
            binding.noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailure() {
        Log.e(TAG,"Error couldn't get data");
        binding.progressBar.setVisibility(View.GONE);
        binding.noData.setVisibility(View.VISIBLE);
        binding.noData.setText(R.string.download_error);
    }
}

