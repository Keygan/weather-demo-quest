package pl.exaco.internship.android.weatherdemo.ui.futureForecast;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.model.FutureWeather;

/**
 * Created by jurki on 08.07.2018.
 */

public interface FutureForecastContract {

    interface Presenter {
        void getWeather(int cityId);

    }

    interface View {
        void onSuccess(List<CityWeather> data);

        void onFailure();
    }
}
