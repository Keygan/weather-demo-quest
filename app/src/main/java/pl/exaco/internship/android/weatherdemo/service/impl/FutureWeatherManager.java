package pl.exaco.internship.android.weatherdemo.service.impl;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CitiesWeather;
import pl.exaco.internship.android.weatherdemo.model.CityWeather;
import pl.exaco.internship.android.weatherdemo.service.RequestCallback;
import pl.exaco.internship.android.weatherdemo.service.api.WeatherApi;
import pl.exaco.internship.android.weatherdemo.service.IFutureWeatherManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EBean(scope = EBean.Scope.Singleton)
public class FutureWeatherManager extends BaseManager implements IFutureWeatherManager {

    private WeatherApi api;

    @AfterInject
    void initService() {
        api = retrofit.create(WeatherApi.class);
    }

    @Override
    public void getFutureWeatherForCity(int cityId, RequestCallback<List<CityWeather>> callback) {
        api.getWeatherForCity(String.valueOf(cityId)).enqueue(new Callback<CitiesWeather>() {
            @Override
            public void onResponse(Call<CitiesWeather> call, Response<CitiesWeather> response) {
                if (null != response && response.body() != null) {
                    callback.onSuccess(response.body().getList());
                } else {
                    callback.onSuccess(new ArrayList<>());
                }
            }

            @Override
            public void onFailure(Call<CitiesWeather> call, Throwable t) {
                callback.onError(t);
            }
        });

    }


}
